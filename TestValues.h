/*
 * TestValuesParser.h
 *
 *  Created on: 24 апр. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_TESTVALUES_H_
#define SYS_TESTVALUES_H_

#include <stdio.h>

#include <SDR/BASE/common.h>

bool test_str_isEqual(const char * val, const char * str);

int test_parse_int(const char * val);
long double test_parse_float(const char * val);

Sample_t test_parse_sample(const char * val);
Size_t test_parse_samples(const char * val, Sample_t * buf, Size_t maxsize);

#endif /* SYS_TESTVALUES_H_ */
