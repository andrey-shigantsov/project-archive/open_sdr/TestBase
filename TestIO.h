/*
 * TestIO.h
 *
 *  Created on: 22 мая 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_TESTBASE_TESTIO_H_
#define SDR_TESTBASE_TESTIO_H_

#include <SDR/BASE/common.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef int TEST_DataId_t;

void test_setExecTimeoutMs(int32_t timeout);

void test_startPaused();
void test_start();
void test_pause();
void test_step();
void test_continue();
void test_reset();
void test_stop();

void test_sys_setAutoWakeupEnable(bool enable);
void test_sys_wakeup();
void test_sys_wakeup_after_timeout();

Bool_t test_sys_WakeupIsAfterTimeout();

Bool_t test_isStarted();
Bool_t test_isRunning();

Bool_t test_set_control_value(TEST_DataId_t id, const char * format, ...);
Bool_t test_set_control_params(TEST_DataId_t id, const char * format, ...);
Bool_t test_set_status_value(TEST_DataId_t id, const char * format, ...);

Bool_t test_plot_clear(TEST_DataId_t plotId);
Bool_t test_set_plot_params(TEST_DataId_t id, const char * format, ...);

Bool_t test_set_samples(TEST_DataId_t id, Sample_t * Buf, Size_t Count);
Bool_t test_set_samples_iq(TEST_DataId_t id, iqSample_t * Buf, Size_t Count);

void test_log_message(const char * format, ...);

#ifdef __cplusplus
}
#endif

#endif /* SDR_TESTBASE_TESTIO_H_ */
